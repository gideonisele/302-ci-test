var should = require("should");
var expect = require('chai').expect;
var cfg = require('../../src/modules/config');
var uri = cfg.DBURL;
var MongoClient = require('mongodb').MongoClient, Logger =
    require('mongodb').Logger;

var ErrorOrSuccessful = require('../../src/modules/result/ErrorOrSuccessful');
var result =  ErrorOrSuccessful.getInstance();

//Logger.setLevel('debug');

describe("mongodB connection", function () {

    describe("fetch data", function () {

        it("should fetch data from db", function (done) {
            MongoClient.connect(uri,function(err, db) {
                if (err) {
                   console.log("run function responseError(err)")
                } else {
                    console.log("run function Successful(data)")
                }
                db.close();
                done();
            });
        });
    });
});