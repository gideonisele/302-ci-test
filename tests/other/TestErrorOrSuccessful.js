var chai = require('chai');
var expect = chai.expect; // we are using the "expect" style of Chai
var ErrorOrSuccessful = require('../../src/modules/result/ErrorOrSuccessful');
var result =  ErrorOrSuccessful.getInstance();

describe('requestError', function() {
  it('requestError should return error if error message are passed in', function() {
    expect(result.responseError('error')).to.equal('error');
  });
});

  describe('requestSuccessful', function() {
  it('requestSuccessful should return data of json from database success if database data are passed in', function() {
    expect(result.responseSuccessful('data')).to.equal('data');
  });
});
