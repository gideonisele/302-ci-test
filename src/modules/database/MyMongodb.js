var MongoClient = require('mongodb').MongoClient;

var Databaseconfig = require('./../config')

var DBURL = Databaseconfig.DBURL
var dbName = Databaseconfig.DBName

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var errorOrSuccessful = require('./../result/ErrorOrSuccessful')
var responseResult = errorOrSuccessful.getInstance()

var r;

app.use(function (req, res, next) {
    console.log(req.body)
    next();
})

class MyMongodb{

    static getInstance(){

        if(!MyMongodb.instance){
            MyMongodb.instance= new MyMongodb();
        }
        return  MyMongodb.instance;
    }

    updateDataBase (filter, update, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)

            db.collection(table).updateOne(filter,{ $set: update}, function (error, result) {
                if (error) {
                    r = responseResult.responseError(error)
                    res.send(r)
                }
                r = responseResult.responseSuccessful('yes')
                res.send(r)
                client.close()
            })
        })
    }

    readDataBase (find, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true }, function (err, client) {
            if (err) throw err ;

            var list= [];
            var db = client.db(dbName)
            var result = db.collection(table).find(find);

            result.each(function (error, doc) {
                if (error) {
                    console.log(responseResult.responseError(error))
                    r = responseResult.responseError(error)
                    res.send(r)
                } else {
                    if (doc != null) {
                        list.push(doc);
                    } else {
                        r = responseResult.responseSuccessful(list)
                        res.send(r)
                        client.close()
                    }

                }
            })
        })
    }

    addDataBase (json, table, res) {
        MongoClient.connect(DBURL,{ useNewUrlParser: true, useUnifiedTopology: true}, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)

            db.collection(table).insertOne(json, function (error, result) {
                if (error) {
                    r = responseResult.responseError(error)
                    res.send(r)
                }
                r = responseResult.responseSuccessful('yes')
                res.send(r)
                client.close()
            })
        })
    }

    delDataBase (del,  table, res) {
        MongoClient.connect(DBURL, {useNewUrlParser: true}, function (err, client) {
            if (err) throw err;

            var db = client.db(dbName)
            db.collection(table).deleteOne(del, function (err, obj) {

                if (error) {
                    r = responseResult.responseError(error)
                    res.send(r)
                }
                r = responseResult.responseSuccessful('yes')
                res.send(r)
                client.close()
            })
        })
    }


    test (){
        MongoClient.connect(DBURL,function(err, db) {
            if (err) {
                throw err;
            } else {
                console.log("successfully connected to the database");
            }
            db.close();
        });
    }
}

module.exports = MyMongodb;